import { createSlice } from "@reduxjs/toolkit";

export const homeSlice = createSlice({
    name: 'counter',
    initialState: {
        url: {},
        genre: {}
    },
    reducers: {
        getApiConfig: (state, action) =>{
            state.url = action.payload
        },
        getGenre: (state, action) =>{
            state.genre = action.payload
        },

    }
})

export const {getApiConfig, getGenre} = homeSlice.actions

export default homeSlice.reducer