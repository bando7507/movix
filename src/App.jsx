import { useEffect, useState } from 'react'
import { fetchDataFromApi } from './utils/api'
import { useSelector, useDispatch } from 'react-redux'
import { getApiConfig } from './store/slice'
import { BrowserRouter, Routes, Route} from 'react-router-dom'
import Home from './views/home/Home'
import Details from './views/details/Details'
import SearchItems from './views/searchItems/SearchItems'
import PageNotFound from './views/404/PageNotFound'
import Header from './components/header/Header'
import Footer from './components/footer/Footer'


function App() {
  const dispatch = useDispatch()

  useEffect(() =>{
    fetchConfig()
  },[])

  const fetchConfig = () =>{
    fetchDataFromApi('/configuration')
    .then(res => {

      const url = {
        backdrop: res.images.base_url + "original",

        poster: res.images.base_url + "original",

        profile: res.images.base_url + "original",
      } 
      dispatch(getApiConfig(url))
    })
  }

  return (
    <>
      <BrowserRouter>
        <Header /> 
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/mediaType/:id' element={<Details />} />
          <Route path='/search/:query' element={<SearchItems />} />
          <Route path='/explore/:mediaType' element={<SearchItems />} />
          <Route path='*' element={<PageNotFound />} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </>
  )
}

export default App
