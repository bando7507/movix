import axios from 'axios'

const BASE_URL = 'https://api.themoviedb.org/3'

const TMDB_TOKEN = import.meta.env.VITE_TOKEN_KEY_API_KEY
const TOKEN_TMDB = import.meta.env.VITE_KEY_API_KEY

const headers = {
    Authorization: 'bearer ' + TOKEN_TMDB,
    TOKEN_TMDB
}

export const fetchDataFromApi = async (url, params) =>{
    try{
        const { data } = await axios.get(BASE_URL + url + '?api_key=' + `${TOKEN_TMDB}`)
        return data
    }catch (err){
        console.log(err);
        return err
    }
}
