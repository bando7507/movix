import React from 'react'
import { useState } from 'react'
import { HiOutlineSearch} from 'react-icons/hi'
import { SlMenu } from 'react-icons/sl'
import { VscChromeClose } from 'react-icons/vsc'
import { useNavigate, useLocation } from 'react-router-dom'
import './header.scss'

import ContentWrapper from '../contentWrapper/ContentWrapper'
import logo from '../../assets/img/movix-logo.png'

const Header = () => {

  const [show, setShow] = useState('top')
  const [lastScrolly, setLastScrolly] = useState(0)
  const [mobile, setMobile] = useState(false)
  const [query, setQuery] = useState('')
  const [showSearch, setShowSearch] = useState('')

  const navigate = useNavigate()
  const location = useLocation()

  const openMobile = () =>{
    setMobile(true)
  }

  return (
    <header className={`header ${mobile ? 'mobileView' : ''}`}>
      <ContentWrapper>
        <div className="logo">
          <img src={logo} alt="movix-logo" />
        </div>
        <ul className="menuItems">
          <li className="menuItem">Movies</li>
          <li className="menuItem">TV Shows</li>
          <li className="menuItem">
            <HiOutlineSearch />
          </li>
        </ul>

        <div className="mobileMenuItems">
          <HiOutlineSearch />
          {mobile ? <VscChromeClose onClick={() => setMobile(false)}/> : <SlMenu onClick={()=>openMobile()}/>}
        </div>
      </ContentWrapper>
    </header>
  )
}

export default Header