import React from 'react'
import './contentwrapper.scss'

const ContentWrapper = ( {children }) => {
  return (
    <div className='contentwrapper'>
        {children}
    </div>
  )
}

export default ContentWrapper