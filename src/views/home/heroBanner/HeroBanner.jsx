import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import './heroBanner.scss'
import useFetch from '../../../hooks/useFetch'
import { useSelector } from 'react-redux'
import Img from '../../../components/lazyLoadingImage/Img'
import ContentWrapper from '../../../components/contentWrapper/ContentWrapper'

import './heroBanner.scss'



const HeroBanner = () => {

    const [background, setBackground] = useState('')
    const [query, setQuery] = useState('')
    const navigate = useNavigate()
    const { data, loading } = useFetch('/movie/upcoming')
    const { url } = useSelector(state => state.counter)

    useEffect(() => {
        if (data) {
            const bg = url.backdrop + data?.results[Math.floor(Math.random() * 20)].backdrop_path;
            setBackground(bg)
        }
    },[data])

    const searchqueryHandle = (event) => {
        if(event.key === 'Enter' && query.length > 0){
            navigate(`/search/${query}`)
        }
    }
  return (
    <div className='heroBanner'>
        {!loading &&         
            <div className="backdrop_img">
                <Img src={background} />
            </div>
        }
        <div className="opacity-layer" />
        <ContentWrapper>
            <div className="heroBannerContainer">
                    <span className='title'>Welcome</span>
                    <span className='subtitle'>Million od movies, TV shows and people to discover.
                    Explore now.
                    </span>
                    <div className="searchInput">
                        <input 
                        type="text" 
                        placeholder='Search for movies or TV shows...'
                        onChange={(e) => setQuery(e.target.value)}
                        onKeyUp={searchqueryHandle}
                        />
                        <button>Search</button>
                    </div>
            </div>
        </ContentWrapper>
    </div>
  )
}

export default HeroBanner